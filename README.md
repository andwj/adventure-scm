
Adventure
=========

This is a simple text adventure game, written in Scheme (R7RS).

It was made as an exercise in learning Scheme, so the code is
probably not very idiomatic, concise or efficient.  It is based
on the adventure game I wrote from scratch when learning Rust
(see http://github.com/andwj/adventure).

The license is CC0 license (i.e. public domain).

