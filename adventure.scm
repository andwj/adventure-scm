;;;
;;; A simple adventure game, in Scheme (R7RS)
;;;
;;; by Andrew Apted, 2018.
;;;
;;; this code is licensed as CC0 (i.e. public domain)
;;;

(import (scheme base)
        (scheme char)
        (scheme cxr)
        (scheme read)
        (scheme write)
)

;; Utility functions ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (displayln str)
  (display str)
  (newline))

(define (table-get table field)
  (letrec ((lookup (lambda (ls)
             (cond
               ((null? ls) #f)
               ((eqv? (car ls) field) (car (cdr ls)))
               (else (lookup (cdr (cdr ls)))) ))))
    (lookup table)))

(define (table-set! table field value)
  (letrec ((lookup (lambda (ls)
             (cond
               ((null? ls) #f)
               ((eqv? (car ls) field) (set-car! (cdr ls) value))
               (else (lookup (cdr (cdr ls)))) ))))
    (lookup table)))

(define (filter pred ls)
  (if (null? ls)
    ls
    (let ((val (car ls)) (rest (cdr ls)))
      (if (pred val)
        (cons val (filter pred rest))
        (filter pred rest) ))))

(define (deep-copy ls)
  (if (null? ls)
    ls
    (let ((val (car ls)))
      (let ((dup (if (list? val) (deep-copy val) val)))
        (cons dup (deep-copy (cdr ls))) ))))

(define (find-by-name ls name)
   (if (null? ls)
     #f
     (let ((table (car ls)))
       (if (equal? name (table-get table 'name))
         table
         (find-by-name (cdr ls) name) ))))


;; Room definitions ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define DIRS '(
  ("n" N) ("north" N)
  ("s" S) ("south" S)
  ("e" E) ("east" E)
  ("w" W) ("west" W)
  ("u" U) ("up" U)
  ("d" D) ("down" D)
))

(define ROOMS (deep-copy '(
  (name "Mountain"
   exits ((N "Forest" Free))
   objects ()
   description
   ("You are standing on a large grassy mountain."
    "To the north you see a thick forest."
    "Other directions are blocked by steep cliffs."))

  (name "Forest"
   exits ((S "Mountain" Free)
          (W "Lake" Free)
          (E "Outside" Crocodile))
   objects ("crocodile" "parrot")
   description
   ("You are in a forest, surrounded by dense trees and shrubs."
    "A wide path slopes gently upwards to the south, and"
    "arrow paths lead east and west."))

  (name "Lake"
   exits ((E "Forest" Free))
   objects ("steak")
   description
   ("You stand on the shore of a beautiful lake, soft sand under"
    "your feet.  The clear water looks warm and inviting."))

  (name "Outside"
   exits ((W "Forest" Free)
          (E "Castle" Key))
   objects ()
   description
   ("The forest is thinning off here.  To the east you can see a"
    "large castle made of dark brown stone.  A narrow path leads"
    "back into the forest to the west."))

  (name "Castle"
   exits ((W "Outside" Free)
          (S "Treasury" Password))
   objects ("guard" "carrot")
   description
   ("You are standing inside a magnificant, opulent castle."
    "A staircase leads to the upper levels, but unfortunately"
    "it is currently blocked off by delivery crates.  A large"
    "wooden door leads outside to the west, and a small door"
    "leads south."))

  (name "Treasury"
   exits ((N "Castle" Free))
   objects ("treasure")
   description
   ("Wow!  This room is full of valuable treasures.  Gold, jewels,"
    "valuable antiques sit on sturdy shelves against the walls."
    "However...... perhaps money isn't everything??"))
)))

(define PASSWORD "snowballs")

(define (room-get-exit room dir)
  (letrec ((exits (table-get room 'exits))
           (lookup (lambda (ls)
             (if (null? ls)
               #f
               (if (eqv? dir (caar ls))
                 (car ls)
                 (lookup (cdr ls)) )))))
    (lookup exits) ))

(define (room-free-exit room dir)
  (let ((exit (room-get-exit room dir)))
    (let ((exit (cdr (cdr exit))))
      (set-car! exit 'Free) )))

(define (room-add-obj room obj)
  (table-set! room 'objects
    (append (table-get room 'objects) (list obj))))

(define (room-remove-obj room obj)
  (table-set! room 'objects
    (filter (lambda (x) (not (equal? x obj))) (table-get room 'objects))))

(define (room-describe name)
  (let ((room (find-by-name ROOMS name)))
    (let ((desc (table-get room 'description)))
      (for-each (lambda (line) (displayln line)) desc))
    (let ((objs (table-get room 'objects)))
      (for-each
        (lambda (obj) (display "There is a ") (display obj) (displayln " here."))
        objs) )))


;; Player definition ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define player-loc "Mountain")

(define player-found-key #f)

(define player-inventory '("sword"))

(define (player-add-obj obj)
  (set! player-inventory (append player-inventory (list obj))))

(define (player-remove-obj obj)
  (set! player-inventory
    (filter (lambda (x) (not (equal? x obj))) player-inventory)))


;; Game logic ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define game-over #f)

(define (welcome-message)
  (newline)
  (displayln "Welcome to a simple adventure game!")
  (newline))

(define (quit-message)
  (displayln "Goodbye!"))

(define (solved-message)
  (displayln "You help yourself to the treasure.")
  (displayln "With your good health and new-found wealth,")
  (displayln "you live happily ever after....")
  (newline)
  (displayln "Congratulations, you solved the game!")
  (newline))

(define (cmd-quit)
  (set! game-over #t))

(define (cmd-help)
  (displayln "Use text commands to walk around and do things.")
  (displayln "Some examples:")
  (displayln "   go north")
  (displayln "   get the rope")
  (displayln "   drop the lantern")
  (displayln "   inventory")
  (displayln "   unlock door")
  (displayln "   kill the serpent")
  (displayln "   quit"))

(define (cmd-inventory)
  (displayln "You are carrying:")
  (if (null? player-inventory)
    (displayln "    nothing")
    (for-each
      (lambda (obj) (display "    a ") (displayln obj))
      player-inventory) ))

(define (cmd-look)
  (newline)
  (room-describe player-loc))

(define (cmd-go nouns)
  (if (null? nouns)
    (displayln "Go where?")
    (let* ((dirname (car nouns))
           (dir (assoc dirname DIRS)))
      (if (not dir)
        (displayln "I don't understand that direction.")
        ;; convert assoc result to a symbol: N, S, E, W (etc)
        (let* ((dir (cadr dir))
               (room (find-by-name ROOMS player-loc))
               (exit (room-get-exit room dir)))
          (if (not exit)
            (displayln "You cannot go that way.")
            (let ((nextroom (cadr exit))
                  (obstacle (caddr exit)))
              (case obstacle
                ((Key) (displayln "The castle door is locked!"))
                ((Crocodile) (displayln "A huge, scary crocodile blocks your path!"))
                ((Password) (displayln "The guard stops you and says \"Hey, you cannot go in there")
                            (displayln "unless you tell me the password!\"."))
                (else ; Free
                  (set! player-loc nextroom)
                  (newline)
                  (room-describe player-loc)) ))))))))

(define (cmd-swim)
  (cond
    ((equal? player-loc "Lake")
      (if player-found-key
        (displayln "You enjoy a nice swim in the lake.");
        (begin
          (displayln "You dive into the lake, enjoy paddling around for a while.")
          (displayln "Diving a bit deeper, you discover a rusty old key!")
          (set! player-found-key #t)
          (player-add-obj "key") )))
    ((equal? player-loc "Outside")
      (displayln "But the moat is full of crocodiles!"))
    (else
      (displayln "There is nowhere to swim here.")) ))

(define (cmd-drop nouns)
  (if (null? nouns)
    (displayln "Drop what?")
    (let ((obj (car nouns))
          (room (find-by-name ROOMS player-loc)))
      (if (not (member obj player-inventory))
        (begin (display "You are not carrying a ") (display obj)
               (displayln "."))
        (begin
          (player-remove-obj obj)
          (room-add-obj room obj)
          (display "You drop the ") (display obj)
          (displayln ".")) ))))

(define (cmd-get nouns)
  (if (null? nouns)
    (displayln "Get what?")
    (let* ((obj (car nouns))
           (room (find-by-name ROOMS player-loc))
           (avail (table-get room 'objects)))
      (if (not (member obj avail))
        (begin (display "You do not see any ") (display obj)
               (displayln " here."))
        (cond
          ((equal? obj "crocodile")
            (displayln "Are you serious?")
            (displayln "The only thing you would get is eaten!"))

          ((equal? obj "parrot")
            (displayln "The parrot nimbly evades your grasp."))

          ((equal? obj "guard")
            (displayln "A momentary blush suggests the guard was flattered."))

          ((equal? obj "treasure")
            (solved-message)
            (set! game-over #t))

          (else
            (room-remove-obj room obj)
            (player-add-obj obj)
            (display "You pick up the ") (display obj)
            (displayln ".")) )))))

(define (cmd-give nouns)
  (if (or (null? nouns) (null? (cdr nouns)))
    (displayln "Give what to whom?")
    (let* ((obj (car nouns))
           (whom (cadr nouns))
           (room (find-by-name ROOMS player-loc)))
      ;; check player has the object
      (if (not (member obj player-inventory))
        (begin (display "You can't give a ") (display obj)
               (displayln ", since you don't have one!"))
        ;; check the recipient is present
        (if (not (member whom (table-get room 'objects)))
          (begin (display "There is no ") (display whom)
                 (displayln " here."))
          (cond
            ((and (equal? obj "carrot") (equal? whom "parrot"))
              (player-remove-obj obj)
              (displayln "The parrot happily starts munching on the carrot.  Every now")
              (display "and then you hear it say \"") (display PASSWORD)
              (displayln "\" as it nibbles away on that")
              (displayln "orange stick.  I wonder who this parrot belonged to?"))

            ((and (equal? obj "steak") (equal? whom "crocodile"))
              (player-remove-obj obj)
              (displayln "You hurl the steak towards the crocodile, which suddenly")
              (displayln "snaps into action, grabbing the steak in its steely jaws")
              (displayln "and slithering off to devour its meal in private.")
              (room-remove-obj room whom)
              (room-free-exit room 'E))

            ((member whom '("parrot" "crocodile" "guard"))
              (display "The ") (display whom)
              (displayln " is not interested."))

            (else
              (displayln "Don't be ridiculous!")) ))))))

(define (cmd-feed nouns)
  (if (or (null? nouns) (null? (cdr nouns)))
    (displayln "Feed what to whom?")
    (cmd-give nouns)))

(define (cmd-open nouns)
  (if (null? nouns)
    (displayln "Open what?")
    (let ((obj (car nouns))
          (room (find-by-name ROOMS player-loc)))
      (cond
        ((not (equal? obj "door"))
          (displayln "You cannot open that."))
        ((not (equal? player-loc "Outside"))
          (displayln "There is no door here."))
        ((not (member "key" player-inventory))
          (displayln "The door is locked!"))
        (else
          (displayln "Carefully you insert the rusty old key in the lock, and turn it.")
          (displayln "Yes!!  The door unlocks!  However the key breaks into several")
          (displayln "pieces and is useless now.")
          (player-remove-obj "key")
          (room-free-exit room 'E)) ))))

(define (cmd-attack nouns)
  (if (null? nouns)
    (displayln "Attack what?")
    (let ((target (car nouns))
          (sword (member "sword" player-inventory)))
      (cond
        ((equal? target "crocodile")
          (displayln "The mere thought of wrestling with that savage beast")
          (displayln "paralyses you with fear!"))
        ((equal? target "guard")
          (if sword
            (begin
               (displayln "You and the guard begin a dangerous sword fight!")
               (displayln "But after ten minutes or so, you are both exhausted and")
               (displayln "decide to call it a draw."))
            (begin
               (displayln "You raise your hands to fight, then notice that the guard")
               (displayln "is carrying a sword, so you shadow box for a while instead."))))
        (else
          (if sword
            (displayln "You swing your sword, but miss!")
            (displayln "You bruise your hand in the attempt."))) ))))

(define (cmd-use nouns)
  (if (null? nouns)
    (displayln "Use what?")
    (let ((obj (car nouns)))
      (if (not (member obj player-inventory))
        (displayln "You cannot use something you don't have.")
        (cond
          ((equal? obj "key")
            (cmd-open '("door")))
          ((equal? obj "sword")
            (displayln "You practise your parry skills."))
          (else
            (displayln "Its lack of utility leads to futility.")) )))))

(define (cmd-say nouns)
  (if (null? nouns)
    (displayln "Say what?")
    (let ((word (car nouns))
          (room (find-by-name ROOMS player-loc)))
      (if (not (equal? word PASSWORD))
        (displayln "Nothing happens.")
        (begin
          (displayln "The guard says \"Welcome Sire!\" and beckons you to enter")
          (displayln "the treasury.")
          (room-free-exit room 'S))) )))


;; Parsing ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (split-words str)
  (let* ((str (string-downcase str))
         (words '())
         (curword "")

         (add-char (lambda (ch)
           (set! curword (string-append curword (string ch)))
           ))

         (add-word (lambda ()
           (unless (equal? curword "")
             (set! words (append words (list curword))) )
           (set! curword "")))

         (process-char (lambda (ch)
           (cond
             ((char-whitespace? ch) (add-word))
             (else (add-char ch)))))
        )

      (string-for-each process-char str)
      ;; ensure the current word is handled
      (add-word)
      words))

;; UGH, cannot use (case ...) with strings!
(define (valid-word? word)
  (not (member word '("a" "an" "the" "to" "with"))))

(define (expand-abbr word)
  (if (equal? word "croc")
    "crocodile"
    word))

(define (sanitize words)
  (filter valid-word?
    (map expand-abbr words)))

(define (user-command verb nouns)
  (cond
    ((member verb '("exit" "quit" "q")) (cmd-quit))
    ((member verb '("help")) (cmd-help))
    ((member verb '("inventory" "invent" "inv" "i")) (cmd-inventory))
    ((member verb '("look" "l")) (cmd-look))
    ((member verb '("go" "walk")) (cmd-go nouns))
    ((assoc verb DIRS) (cmd-go (list verb)))

    ((member verb '("swim" "dive")) (cmd-swim))
    ((member verb '("drop")) (cmd-drop nouns))
    ((member verb '("get" "take")) (cmd-get nouns))
    ((member verb '("give" "offer")) (cmd-give nouns))
    ((member verb '("feed")) (cmd-feed nouns))
    ((member verb '("open" "unlock")) (cmd-open nouns))
    ((member verb '("kill" "attack" "hit" "fight")) (cmd-attack nouns))
    ((member verb '("use" "apply")) (cmd-use nouns))
    ((member verb '("say" "speak" "tell")) (cmd-say nouns))
    ((equal? verb PASSWORD) (cmd-say (list PASSWORD)))

    (else
      (display "I don't understand '") (display verb)
      (displayln "'")) ))

(define (parse-user-command str)
  (let ((words (split-words str)))
    ;; ignore empty lines
    (unless (null? words)
      (let ((words (sanitize words)))
        (cond
          ((null? words) (displayln "Huh?"))
          (else
            (let ((verb (car words))
                  (nouns (cdr words)))
              (user-command verb nouns))) )))))

(define (read-and-process-command)
  (display "> ")  ; the prompt
  (flush-output-port)
  (let ((inp (read-line)))
    (cond
      ;; just quit on EOF [ nothing else we can do! ]
      ((eof-object? inp) (set! game-over #t) (newline))
      (else (parse-user-command inp)) )))

(define (main)
  (welcome-message)
  (room-describe player-loc)
  (do ()
      (game-over)
    (read-and-process-command))
  (quit-message)
)

(main)

;;--- editor settings ---
;; vi:ts=2:sw=2:expandtab
