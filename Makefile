INTERPRETER=chibi-scheme

all:
	@echo "nothing to compile, just use: make run"

run:
	@$(INTERPRETER) adventure.scm
